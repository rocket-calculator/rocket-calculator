#ifndef PARSERUTIL_H
#define PARSERUTIL_H

#pragma once

class QString;

class ParserUtil
{
    ParserUtil() = delete;

public:
    static QString getText(QString name, QString text);
    static int getNumber(QString name, QString text);
    static double getReal(QString name, QString text);
};

#endif // PARSERUTIL_H
