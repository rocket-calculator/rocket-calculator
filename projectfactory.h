#ifndef PROJECTFACTORY_H
#define PROJECTFACTORY_H

#pragma once

#include <QList>
#include <QSharedPointer>

class QString;

namespace ksp {
class PartSchematic;
}

class ProjectFactory
{
public:
    ProjectFactory() = delete;
    ProjectFactory(const ProjectFactory&) = delete;
    ProjectFactory& operator=(const ProjectFactory&) = delete;

    static QList<QSharedPointer<const ksp::PartSchematic>> createFromData(QString dataPath, QString outputFilePath);

};

#endif // PROJECTFACTORY_H
