#include "project.h"

#include <QList>
#include <QSharedPointer>

#include "ksp/part.h"

class ProjectData
{
public:
    QList<QSharedPointer<const ksp::PartSchematic> > parts;
};

Project::Project(QObject *parent)
    : QObject(parent)
    , m_data(new ProjectData())
{
}

Project::~Project()
{
}
