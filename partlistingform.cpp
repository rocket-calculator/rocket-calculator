#include "partlistingform.h"
#include "ui_partlistingform.h"

#include <QListWidget>
#include <QFile>
#include <QPlainTextEdit>

#include "ksp/partschematic.h"

static const int s_nameRole = QListWidgetItem::UserType + 10;

PartListingForm::PartListingForm(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::PartListingForm)
{
    ui->setupUi(this);

    ui->openButton->setIcon(style()->standardIcon(QStyle::SP_FileIcon));

    QListWidget* allPartsList = new QListWidget();
    QListWidget* podsList = new QListWidget();
    QListWidget* propulsionList = new QListWidget();
    QListWidget* controlList = new QListWidget();
    QListWidget* structuralList = new QListWidget();
    QListWidget* aeroList = new QListWidget();
    QListWidget* utilityList = new QListWidget();
    QListWidget* scienceList = new QListWidget();

    m_listWidgets.append(allPartsList);
    m_listWidgets.append(podsList);
    m_listWidgets.append(propulsionList);
    m_listWidgets.append(controlList);
    m_listWidgets.append(structuralList);
    m_listWidgets.append(aeroList);
    m_listWidgets.append(utilityList);
    m_listWidgets.append(scienceList);

    ui->tabWidget->addTab(allPartsList, tr("All"));
    ui->tabWidget->addTab(podsList, tr("Pods"));
    ui->tabWidget->addTab(propulsionList, tr("Propulsion"));
    ui->tabWidget->addTab(controlList, tr("Control"));
    ui->tabWidget->addTab(structuralList, tr("Structural"));
    ui->tabWidget->addTab(aeroList, tr("Aerodynamics"));
    ui->tabWidget->addTab(utilityList, tr("Utility"));
    ui->tabWidget->addTab(scienceList, tr("Science"));

    connect(ui->tabWidget, SIGNAL(currentChanged(int)), SLOT(refreshShownPart()));
    for (QListWidget *listWidget : m_listWidgets)
    {
        connect(listWidget, SIGNAL(currentItemChanged(QListWidgetItem*,QListWidgetItem*)), SLOT(refreshShownPart()));
    }
}

PartListingForm::~PartListingForm()
{
    delete ui;
}

void PartListingForm::setParts(QList<QSharedPointer<const ksp::PartSchematic> > &&parts)
{
    m_parts = std::move(parts);

    for (QSharedPointer<const ksp::PartSchematic> part : m_parts)
    {
        QListWidgetItem *item = new QListWidgetItem(part->title(), m_listWidgets[0]);
        item->setData(s_nameRole, part->name());
        item = new QListWidgetItem(part->title(), m_listWidgets[int(part->category())]);
        item->setData(s_nameRole, part->name());
    }

    for (QListWidget *listWidget : m_listWidgets)
    {
        listWidget->sortItems();
    }
}

void PartListingForm::refreshShownPart()
{
    int currentTab = ui->tabWidget->currentIndex();
    if (currentTab >= 0 && currentTab < m_listWidgets.size())
    {
        auto *item = m_listWidgets[currentTab]->currentItem();
        if (item)
        {
            QString partName = item->data(s_nameRole).toString();
            for (QSharedPointer<const ksp::PartSchematic> part : m_parts)
            {
                if (part->name() == partName)
                {
                    ui->titleLabel->setText(part->title());
                    ui->descriptionField->setText(part->description());
                    ui->manufacturerField->setText(part->manufacturer());
                    ui->costField->setText(QString::number(part->cost()));
                    ui->massField->setText(QString::number(part->mass()));
                    break;
                }
            }
        }
    }
}

void PartListingForm::on_openButton_clicked()
{
    int currentTab = ui->tabWidget->currentIndex();
    if (currentTab >= 0 && currentTab < m_listWidgets.size())
    {
        auto *item = m_listWidgets[currentTab]->currentItem();
        if (item)
        {
            QString partName = item->data(s_nameRole).toString();
            for (QSharedPointer<const ksp::PartSchematic> part : m_parts)
            {
                if (part->name() == partName)
                {
                    QFile partFile(part->filePath());
                    if (partFile.exists() && partFile.open(QIODevice::ReadOnly))
                    {
                        QPlainTextEdit *textEdit = new QPlainTextEdit(partFile.readAll());
                        textEdit->setReadOnly(true);
                        textEdit->show();
                    }
                }
            }
        }
    }
}
