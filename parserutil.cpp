#include "parserutil.h"

#include <QString>
#include <QRegularExpression>

static const QString s_baseExpression = "^\\s*%1\\s*=\\s*(%2)\\s*\\n?$";

QString ParserUtil::getText(QString name, QString text)
{
    QRegularExpression rx(s_baseExpression.arg(name, ".*?"), QRegularExpression::MultilineOption);
    QRegularExpressionMatch matchResult = rx.match(text);
    if (matchResult.hasMatch())
    {
        return matchResult.captured(1).trimmed();
    }
    return QString();
}

int ParserUtil::getNumber(QString name, QString text)
{
    QRegularExpression rx(s_baseExpression.arg(name, "\\-?\\d+"), QRegularExpression::MultilineOption);
    QRegularExpressionMatch matchResult = rx.match(text);
    if (matchResult.hasMatch())
    {
        return matchResult.captured(1).trimmed().toInt();
    }
    return int();
}

double ParserUtil::getReal(QString name, QString text)
{
    QRegularExpression rx(s_baseExpression.arg(name, "\\d*\\.?\\d*"), QRegularExpression::MultilineOption);
    QRegularExpressionMatch matchResult = rx.match(text);
    if (matchResult.hasMatch())
    {
        return matchResult.captured(1).trimmed().toDouble();
    }
    return double();
}
