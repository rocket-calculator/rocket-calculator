#include "projectfactory.h"

#include <QDir>
#include <QDirIterator>
#include <QFileInfo>
#include <QFile>
#include <QRegularExpression>
#include <QSharedPointer>

#include "parserutil.h"

#include "ksp/partschematic.h"
#include "ksp/category.h"
#include "ksp/module.h"
#include "ksp/moduletype.h"


namespace
{

QList<ksp::Module*> parseModules(const QString& text)
{
    QList<ksp::Module*> resultList;
    return std::move(resultList);
}

QSharedPointer<ksp::PartSchematic> parsePartFile(QFile& partFile)
{
    QString text(partFile.readAll());

    // ==DATA TO FILL==
    QString name = ParserUtil::getText("name", text);
    QString author = ParserUtil::getText("author", text);
    QString title = ParserUtil::getText("title", text);
    QString manufacturer = ParserUtil::getText("manufacturer", text);
    QString description = ParserUtil::getText("description", text);
    int cost = ParserUtil::getNumber("cost", text);
    double mass = ParserUtil::getReal("mass", text);
    ksp::Category category = ksp::util::categoryFromString(ParserUtil::getText("category", text));
    QList<ksp::Module*> modules = parseModules(text);
    // ================

    return QSharedPointer<ksp::PartSchematic>::create(partFile.fileName(), name, author, title, manufacturer, description, cost, mass, category, modules);
}

} // anonymous namespace

QList<QSharedPointer<const ksp::PartSchematic> > ProjectFactory::createFromData(QString dataPath, QString outputFilePath)
{
    QList<QSharedPointer<const ksp::PartSchematic>> resultList;

    QDir rootDataDir(dataPath);
    if (!rootDataDir.exists())
    {
        // TODO: assert or something
        return resultList;
    }

    QFileInfo outputFileInfo(outputFilePath);
    if (outputFileInfo.exists() && !outputFileInfo.isWritable())
    {
        // TODO: assert, cannot write to output
        return resultList;
    }

    QStringList filters;
    filters << "*.cfg";
    QDirIterator dataIterator(rootDataDir.path(), filters, QDir::Files|QDir::Readable, QDirIterator::Subdirectories);
    while (dataIterator.hasNext())
    {
        QString filePath = dataIterator.next();
        QFile partFile(filePath);
        partFile.open(QIODevice::ReadOnly);
        QString header(partFile.readLine().trimmed());
        if (!header.compare("PART"))
        {
            auto part = parsePartFile(partFile);
            if (part->isValid())
                resultList.append(part);
        }
    }

    return std::move(resultList);
}
