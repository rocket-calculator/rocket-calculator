#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "newprojectdialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_action_Exit_triggered()
{
    close();
}

void MainWindow::on_action_New_Project_triggered()
{
    NewProjectDialog *newProjectDialog = new NewProjectDialog(this);
    newProjectDialog->show();
}
