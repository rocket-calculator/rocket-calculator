#ifndef NEWPROJECTDIALOG_H
#define NEWPROJECTDIALOG_H

#include <QDialog>

namespace Ui {
class NewProjectDialog;
}

class NewProjectDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewProjectDialog(QWidget *parent = 0);
    ~NewProjectDialog();

public slots:
    void sourceTypeChanged();

private slots:
    void on_chooseSourceButton_clicked();

    void on_buttonBox_accepted();

private:
    bool checkForValidFields() const;

    Ui::NewProjectDialog *ui;
};

#endif // NEWPROJECTDIALOG_H
