#include "newprojectdialog.h"
#include "ui_newprojectdialog.h"

#include "projectfactory.h"

#include "partlistingform.h"

#include <QFileDialog>
#include <QDir>

static QString getDefaultPath()
{
    QString defaultPath;
#if defined(Q_OS_WIN)
    QDir defaultPlatformDir("C:\\Program Files (x86)\\Steam\\SteamApps\\common\\Kerbal Space Program\\GameData");
#elif defined(Q_OS_LINUX)
    QDir defaultPlatformDir(QDir::homePath() + "/.local/share/Steam/SteamApps/common/Kerbal Space Program/GameData");
#else
    QDir defaultPlatformDir(QDir::home());
#endif
    if (defaultPlatformDir.exists())
    {
        defaultPath = defaultPlatformDir.absolutePath();
    }

    return defaultPath;
}

NewProjectDialog::NewProjectDialog(QWidget *parent)
    : QDialog(parent)
    , ui(new Ui::NewProjectDialog)
{
    ui->setupUi(this);
    connect(ui->createFromDataButton, SIGNAL(clicked()), SLOT(sourceTypeChanged()));
    connect(ui->createFromExistingButton, SIGNAL(clicked()), SLOT(sourceTypeChanged()));
    connect(ui->createFromDefaultButton, SIGNAL(clicked()), SLOT(sourceTypeChanged()));

    sourceTypeChanged();

    adjustSize();
    setMinimumSize(size());
}

NewProjectDialog::~NewProjectDialog()
{
    delete ui;
}

bool NewProjectDialog::checkForValidFields() const
{
    // TODO: verify that all the fields are valid and display error messages
    return true;
}

void NewProjectDialog::sourceTypeChanged()
{
    ui->sourceField->clear();
    bool canChooseSource = !ui->createFromDefaultButton->isChecked();
    ui->sourceField->setEnabled(canChooseSource);
    ui->chooseSourceButton->setEnabled(canChooseSource);

    if (ui->createFromDataButton->isChecked())
    {
        ui->sourceField->setText(getDefaultPath());
    }
}

void NewProjectDialog::on_chooseSourceButton_clicked()
{
    QString sourcePath;
    if (ui->createFromExistingButton->isChecked())
    {
        sourcePath = QFileDialog::getOpenFileName(this, tr("Open existing project file"), QDir::homePath(), tr("Rocket Calculator Project File (*.rkc)"));
    }
    else if (ui->createFromDataButton->isChecked())
    {
        sourcePath = QFileDialog::getExistingDirectory(this, tr("Kerbal GameData folder"), getDefaultPath());
    }
    else
    {
        // shouldn't be there
        // TODO: assert or something
        return;
    }

    ui->sourceField->setText(sourcePath);
}

void NewProjectDialog::on_buttonBox_accepted()
{
    if (!checkForValidFields())
    {
        return;
    }

    if (ui->createFromDataButton->isChecked())
    {
        auto data = ProjectFactory::createFromData(ui->sourceField->text(), ui->locationEdit->text());
        // TODO: remove
        PartListingForm *listForm = new PartListingForm();
        listForm->setParts(std::move(data));
        listForm->show();
    }
}
