#ifndef PROJECT_H
#define PROJECT_H

#pragma once

#include <QObject>
#include <QScopedPointer>

class ProjectData;

class Project : public QObject
{
    Q_OBJECT

    QScopedPointer<ProjectData> m_data;

public:
    explicit Project(QObject *parent = 0);
    virtual ~Project();

signals:

public slots:

};

#endif // PROJECT_H
