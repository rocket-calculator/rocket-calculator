#ifndef KSP_MODULETYPE_H
#define KSP_MODULETYPE_H

#pragma once

class QString;

namespace ksp
{

enum class ModuleType : int
{
    Invalid,
    Engine,
    Gimbal,
    Decoupler,
    FlagDecal,
};

namespace util
{
ModuleType moduleTypeFromString(const QString& str);
ModuleType moduleTypeFromCStr(const char* cstr);
}

}

#endif // KSP_MODULETYPE_H
