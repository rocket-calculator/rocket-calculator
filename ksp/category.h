#ifndef KSP_CATEGORY_H
#define KSP_CATEGORY_H

#pragma once

class QString;

namespace ksp
{

enum class Category : int
{
    Invalid,
    Pods,
    Propulsion,
    Control,
    Structural,
    Aero,
    Utility,
    Science,
};

namespace util
{
Category categoryFromString(const QString& str);
Category categoryFromCStr(const char* cstr);
}

}

#endif // KSP_CATEGORY_H
