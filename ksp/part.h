#ifndef KSP_PART_H
#define KSP_PART_H

#pragma once

#include <QString>
#include <QList>
#include <QSharedPointer>

namespace ksp
{

class PartSchematic;

class Part
{
    QSharedPointer<PartSchematic> m_schema;

public:
    Part() = delete;
    Part(const Part&) = delete;
    Part& operator=(const Part&) = delete;

    explicit Part(const QSharedPointer<PartSchematic> &schema);
    ~Part();

    const QSharedPointer<PartSchematic>& schema() const { return m_schema; }
};

}

#endif // KSP_PART_H
