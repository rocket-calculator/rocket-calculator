#ifndef KSP_ENGINEMODULE_H
#define KSP_ENGINEMODULE_H

#pragma once

#include "module.h"

namespace ksp
{

class EngineModule : public Module
{
public:
    static ModuleType Type();

    EngineModule();
};

}

#endif // KSP_ENGINEMODULE_H
