#include "category.h"

#include <QString>

#include "crc32util.h"

namespace ksp
{

Category util::categoryFromString(const QString& str)
{
    return categoryFromCStr(str.toLatin1().constData());
}

Category util::categoryFromCStr(const char *cstr)
{
    uint32_t hash = crc32(cstr);

    switch (hash)
    {
    case crc32("Pods"):
        return Category::Pods;
    case crc32("Propulsion"):
        return Category::Propulsion;
    case crc32("Control"):
        return Category::Control;
    case crc32("Structural"):
        return Category::Structural;
    case crc32("Aero"):
        return Category::Aero;
    case crc32("Utility"):
        return Category::Utility;
    case crc32("Science"):
        return Category::Science;
    }

    return Category::Invalid;
}

}
