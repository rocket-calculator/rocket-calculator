#ifndef KSP_MODULE_H
#define KSP_MODULE_H

#pragma once

namespace ksp
{

enum class ModuleType : int;

class Module
{
    ModuleType m_moduleType;

public:
    Module() = delete;
    Module(const Module&) = delete;
    Module& operator=(const Module&) = delete;
    virtual ~Module() = 0;

    ModuleType getType() const { return m_moduleType; }

protected:
    Module(ModuleType moduleType);
};

}

#endif // KSP_MODULE_H
