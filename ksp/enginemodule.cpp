#include "enginemodule.h"
#include "moduletype.h"

namespace ksp
{

ModuleType EngineModule::Type() { return ModuleType::Engine; }

EngineModule::EngineModule()
    : Module(ModuleType::Engine)
{
}

}
