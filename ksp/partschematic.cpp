#include "partschematic.h"

#include "category.h"
#include "module.h"

namespace ksp
{

PartSchematic::PartSchematic(QString filePath, QString name, QString author, QString title, QString manufacturer, QString description, int cost, double mass, Category category, QList<Module*> modules)
    : m_filePath(filePath)
    , m_name(name)
    , m_author(author)
    , m_title(title)
    , m_manufacturer(manufacturer)
    , m_description(description)
    , m_cost(cost)
    , m_mass(mass)
    , m_category(category)
    , m_modules(modules)
{
}

PartSchematic::~PartSchematic()
{
    for (Module *module: m_modules)
    {
        delete module;
    }
}

bool PartSchematic::isValid() const
{
    if (m_category == Category::Invalid)
        return false;

    return true;
}

const Module* PartSchematic::getModule(ModuleType moduleType) const
{
    for (const Module *module: m_modules)
    {
        if (module->getType() == moduleType)
        {
            return module;
        }
    }

    return nullptr;
}

}
