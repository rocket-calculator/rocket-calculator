#include "moduletype.h"

#include <QString>

#include "crc32util.h"

namespace ksp
{

ModuleType util::moduleTypeFromString(const QString &str)
{
    return moduleTypeFromCStr(str.toLatin1().constData());
}

ModuleType util::moduleTypeFromCStr(const char *cstr)
{
    uint32_t hash = crc32(cstr);

    switch (hash)
    {
    case crc32("ModuleEnginesFX"):
        return ModuleType::Engine;
    case crc32("ModuleGimbal"):
        return ModuleType::Gimbal;
    case crc32("ModuleDecoupler"):
        return ModuleType::Decoupler;
    case crc32("FlagDecal"):
        return ModuleType::FlagDecal;
    }

    return ModuleType::Invalid;
}

}
