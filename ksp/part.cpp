#include "part.h"

#include "partschematic.h"

namespace ksp
{

Part::Part(const QSharedPointer<PartSchematic> &schema)
    : m_schema(schema)
{
}

Part::~Part()
{
}

}
