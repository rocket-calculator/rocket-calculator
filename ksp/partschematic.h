#ifndef KSP_PARTSCHEMATIC_H
#define KSP_PARTSCHEMATIC_H

#pragma once

#include <QString>
#include <QList>

namespace ksp
{

enum class Category : int;
class Module;
enum class ModuleType;

class PartSchematic
{
    QString m_filePath;
    QString m_name;
    QString m_author;
    QString m_title;
    QString m_manufacturer;
    QString m_description;
    int m_cost;
    double m_mass;
    Category m_category;
    QList<Module*> m_modules;

public:
    PartSchematic() = delete;
    PartSchematic(const PartSchematic&) = delete;
    PartSchematic& operator=(const PartSchematic&) = delete;

    PartSchematic(QString filePath, QString name, QString author, QString title, QString manufacturer, QString description,
         int cost, double mass, Category category, QList<Module*> modules);
    ~PartSchematic();

    bool isValid() const;

    const QString& filePath() const { return m_filePath; }
    const QString& name() const { return m_name; }
    const QString& title() const { return m_title; }
    const QString& manufacturer() const { return m_manufacturer; }
    const QString& description() const { return m_description; }
    int cost() const { return m_cost; }
    double mass() const { return m_mass; }
    Category category() const { return m_category; }

    const Module* getModule(ModuleType moduleType) const;
    template <class T>
    const T* getModule() const
    {
        return static_cast<const T*>(getModule(T::Type()));
    }
};

}

#endif // KSP_PARTSCHEMATIC_H
