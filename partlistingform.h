#ifndef PARTLISTINGFORM_H
#define PARTLISTINGFORM_H

#pragma once

#include <QWidget>
#include <QList>
#include <QSharedPointer>

class QListWidget;

namespace ksp {
class PartSchematic;
}

namespace Ui {
class PartListingForm;
}

class PartListingForm : public QWidget
{
    Q_OBJECT

public:
    explicit PartListingForm(QWidget *parent = 0);
    ~PartListingForm();

    void setParts(QList<QSharedPointer<const ksp::PartSchematic>> &&parts);

public slots:
    void refreshShownPart();

private slots:
    void on_openButton_clicked();

private:
    Ui::PartListingForm *ui;
    QList<QListWidget*> m_listWidgets;
    QList<QSharedPointer<const ksp::PartSchematic>> m_parts;
};

#endif // PARTLISTINGFORM_H
