#-------------------------------------------------
#
# Project created by QtCreator 2014-09-09T20:58:21
#
#-------------------------------------------------

QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = RocketCalculator
TEMPLATE = app


SOURCES += \
    main.cpp\
    mainwindow.cpp \
    ksp/part.cpp \
    ksp/module.cpp \
    ksp/enginemodule.cpp \
    newprojectdialog.cpp \
    projectfactory.cpp \
    ksp/moduletype.cpp \
    ksp/category.cpp \
    parserutil.cpp \
    partlistingform.cpp \
    project.cpp \
    ksp/partschematic.cpp

HEADERS  += \
    mainwindow.h \
    ksp/part.h \
    ksp/category.h \
    ksp/module.h \
    ksp/enginemodule.h \
    ksp/moduletype.h \
    newprojectdialog.h \
    projectfactory.h \
    crc32util.h \
    parserutil.h \
    partlistingform.h \
    project.h \
    ksp/partschematic.h

FORMS    += \
    mainwindow.ui \
    newprojectdialog.ui \
    partlistingform.ui

CONFIG += c++11 mobility
MOBILITY = 

